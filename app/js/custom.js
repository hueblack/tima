$(document).ready( function () {
	
	// lend slide
	if ($('.slide-lend').length){
		$('.slide-lend').slick({
		    autoplay: true,
		    arrows: false,
		    dots: false,
		    infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
		    centerPadding: "10px",
		    swipe: true,
		    swipeToSlide:true,
		    touchMove: true,
		    vertical: true,
		    speed: 1000,
		    autoplaySpeed: 300000,
		    adaptiveHeight: true
		  });
	}
	// borrow slide
	if ($('.slide-borrow').length){
		$('.slide-borrow').slick({
		    autoplay: true,
		    arrows: false,
		    dots: false,
		    infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
		    centerPadding: "10px",
		    swipe: true,
		    swipeToSlide:true,
		    touchMove: true,
		    vertical: true,
		    speed: 1000,
		    autoplaySpeed: 3000,
		    adaptiveHeight: true
		  });
	}

	// news home slide
	if ($('.ul-news').length){
		$('.ul-news').slick({
			autoplay: true,
		    arrows: false,
		    dots: false,
		  	infinite: true,
		  	slidesToShow: 4,
		  	slidesToScroll: 4,
		  	speed: 800,
		    autoplaySpeed: 3000,
		    responsive: [
			    {
			      breakpoint: 1199,
			      settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3
			      }
			    },
			    {
			      breakpoint: 991,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    },
			    {
			      breakpoint: 767,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			         autoplaySpeed: 3000
			      }
			    }
			]
		});
	}

	// login
	
    if ($('.left-login').length){
		$('.left-login').slick({
		  dots: true,
		  autoplay: true,
		  arrows: false,
		  infinite: true,
		  fade: true,
		  speed: 800,
		  slidesToShow: 1,
		  autoplaySpeed: 3000
		});
					
	}


	// setup
	
    if ($('.left-setup').length){
		$('.left-setup').slick({
		  dots: false,
		  arrows:true,
		  autoplay: true,
		  infinite: true,
		  fade: true,
		  speed: 800,
		  slidesToShow: 1,
		  autoplaySpeed: 3000
		});
					
	}
    
	
	 
   
});